<?php

namespace App\Entity;

class Circle
{
    /**
     * @var string
     */
    private const TYPE = 'circle';

    /**
     * string
     */
    private $type;

    /**
     * float
     */
    private $radius;

    /**
     * float
     */
    private $surface;

    /**
     * @var float
     */
    private $circumference;

    /**
     * @throws InvalidArgumentException Ukoliko je radius neispravan (manji ili jednak nuli)
     */
    public function __construct(float $radius)
    {
        if ($radius <= 0) {
            throw new \InvalidArgumentException(sprintf(
                'Radius "%s" mora biti veći od nule.', 
                $radius
            ));
        }
        $this->type = self::TYPE;
        $this->radius = $radius;
        $this->surface = round($this->calculateSurface($radius), 2);
        $this->circumference = round($this->calculateCircumference($radius), 2);
    }

    public function getJSONParameters(): array
    {
        return [
            'type' => $this->type,
            'radius' => $this->radius,
            'surface' => $this->surface,
            'circumference' => $this->circumference,
        ];
    }

    public function getSurface(): float
    {
        return $this->surface;
    }

    public function getCircumference(): float
    {
        return $this->circumference;
    }

    private function calculateSurface(float $radius): float
    {
        return pow($radius, 2) * pi();
    }

    private function calculateCircumference(float $radius): float
    {
        return 2 * $radius * pi();
    }
}
