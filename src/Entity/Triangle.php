<?php

namespace App\Entity;

class Triangle
{
    /**
     * @var string
     */
    private const TYPE = 'triangle';

    /**
     * string
     */
    private $type;

    /**
     * float
     */
    private $a;

    /**
     * float
     */
    private $b;

    /**
     * float
     */
    private $c;

    /**
     * float
     */
    private $surface;

    /**
     * @var float
     */
    private $circumference;

    /**
     * @throws InvalidArgumentException Ukoliko je omjer stranica neispravan
     */
    public function __construct(float $a, float $b, float $c)
    {
    	$sides = [$a, $b, $c];
    	$rsortedSides = rsort($sides);
        if ($sides[0] >= $sides[1] + $sides[2]) {
            throw new \InvalidArgumentException(sprintf(
                'Najduža stranica "%s" mora biti kraća od zbroja ostalih dvaju stranica "%s" i "%s".', 
                $sides[0],
                $sides[1],
                $sides[2]
            ));
        }
        $this->type = self::TYPE;
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
        $this->surface = round($this->calculateSurface($a, $b, $c), 2);
        $this->circumference = round($this->calculateCircumference($a, $b, $c), 2);
    }

	public function getJSONParameters(): array
	{
		return [
			'type' => 'triangle',
			'a' => $this->a,
			'b' => $this->b,
			'c' => $this->c,
			'surface' => round($this->calculateSurface($this->a, $this->b, $this->c), 2),
			'circumference' => round($this->calculateCircumference($this->a, $this->b, $this->c), 2),
		];
	}

	public function getSurface(): float
	{
		return $this->surface;
	}

	public function getCircumference(): float
	{
		return $this->circumference;
	}

	private function calculateSurface(float $a, float $b, float $c): float
	{
		$s = ($a + $b + $c)/2;
		return sqrt($s * ($s - $a) * ($s - $b) * ($s - $c));
	}

	private function calculateCircumference(float $a, float $b, float $c): float
	{
		return $a + $b + $c;
	}
}
